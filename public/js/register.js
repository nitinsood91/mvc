// function myfunction(){

// var input= document.getElementsByClassName("input");
// // .name[0].attributes[0].value;
// var i ;
// for(i=0;i<input.length;i++){
// 	x = input[i].attributes[3].value;
// 	if(input[i].value.length < 1 ){
//   	document.getElementById("validate_"+ x).innerHTML=input[i] + "is required";
//  }
// }

 

// }



// function validateName(){
// 	var name = document.getElementById("name").value;
// 	if(name.length < 1){
// 		document.getElementById("validate_name").innerHTML="Name is required";

// 	}
// 	else {
// 		document.getElementById("validate_name").innerHTML="";
// 	}


// }


// function validateEmail(){
// 	var email = document.getElementById("email").value;
// 	if(email.length < 1){
// 		document.getElementById("validate_email").innerHTML="Email is required";

// 	}
// 	else {
// 		document.getElementById("validate_email").innerHTML="";
// 	}


// }
// function validateContact(){
// 	var ph = document.getElementById("contact_number").value;
// 	if(ph.length < 1){
// 		document.getElementById("validate_contact_number").innerHTML="Contact is required";

// 	}
// 	else {
// 		document.getElementById("validate_contact_number").innerHTML="";
// 	}


// }
// function validatePassword(){
// 	var pwd = document.getElementById("password").value;
	
// 	if(pwd.length < 8 && pwd.length > 0){
// 		document.getElementById("validate_password").innerHTML="Password length should be atleast 8 characters ";
		
// 	}
// 	else if(pwd.length < 1){
// 		document.getElementById("validate_password").innerHTML="Password is required";

// 	}
// 	else {
// 		document.getElementById("validate_password").innerHTML="";
// 	}	

// }

// function validateConfirmPassword(){
// 	var pwd = document.getElementById("password").value;
// 	var cpwd = document.getElementById("confirm_password").value;
// 	if(pwd != cpwd){
// 		document.getElementById("validate_confirm_password").innerHTML="Password does not match";

// 	}
// 	else if(cpwd.length < 1){
// 		document.getElementById("validate_confirm_password").innerHTML="Password is required";

// 	}
// 	else {
// 		document.getElementById("validate_confirm_password").innerHTML="";
// 	}


// }


// function validateLocality(){
// 	var locality = document.getElementById("locality").value;
	
// 	if(locality.length < 1 ){
// 		document.getElementById("validate_locality").innerHTML="locality is required";
// 	}
// 	else {
// 		document.getElementById("validate_locality").innerHTML="";
// 	}


// }
function validateName(field){
	// var field=document.getElementById("name");
	validateEmpty(field);
}

function validateEmail(field){
	validateEmpty(field);
}

function validateContact(field){
	validateEmpty(field);
}

function validatePassword(field){
	validateEmpty(field);
	validatePwd(field);
}

function validateConfirmPassword(field){
	validateEmpty(field);
	validateConfirmPwd(field);
}

function validateLocality(field){
	validateEmpty(field);
}

function validateEmpty(field){
	var value = field.value;
	var name = field.getAttribute("name");
	if(value.length < 1){
		document.getElementById("validate_"+name).innerHTML=name + " is required";

	}
	else {
		document.getElementById("validate_name").innerHTML="";
	}
}

function validatePwd(field){
	var value = field.value;
	var name = field.getAttribute("name");
	if(value.length < 8 && value.length > 0){
		document.getElementById("validate_"+name).innerHTML= name+" length should be atleast 8 characters";
	}
	else if(value.length < 1){
		document.getElementById("validate_" + name).innerHTML=name + " is required";
	}
	else{
		document.getElementById("validate_"+name).innerHTML="";
	}

}

function validateConfirmPwd(field){
	// var value_cpwd=field.value;
	// var value_pwd=document.getElementById("password").value;
	// var name=field.getAttribute("name");
	// if(value_pwd != value_cpwd){
	// 	document.getElementById("validate_"+name).innerHTML="Passwords does not match";

	// }
	// else if (value_cpwd.length < 1){
	// 	document.getElementById("validate_"+name).innerHTML="Password is required";
	// }
	// else{
	// 	document.getElementById("validate_"+name).innerHTML="";
	// }

	validator.validate(
		field,
		[
			{
				rule : "required",
				message : "Confirm Password is required"
			},
			{
				rule : "match",
				operand : document.getElementById("password").value,
				message : "Confirm Password must be same as password"
			},
		]
	);
}
function submitValidation(){
	validateName();
	validateEmail();
	validateContact();
	validatePassword();
	validateConfirmPassword();
	validateLocality();
	

	return false;


}


var validator = {
	rules : {
		required : function(value) {
			if(value.length > 0) {
				return true;
			}

			return false;
		},
		number : function(value) {
			if(typeof value == "number") {
				return true;
			}
			return false;
		},
		match : function(value1, value2) {
			if(value1 == value2) {
				return true;
			}
			return false;
		},
		minLength : function(value, length) {
			if(value.length >= length) {
				return true;
			}

			return false;
		},
		maxLength : function(value, length) {
			if(value.length <= length) {
				return true;
			}

			return false;
		},
		length : function(value, length) {
			if(value.length == length) {
				return true;
			}

			return false;
		}
	},
	validate : function(field, fieldRules) {

		fieldRules.some(function(validation){
			var ruleFunction = validator.rules[validation.rule];

			var fieldIsValid = true;
			if(validation.hasOwnProperty("operand")) {
				fieldIsValid = ruleFunction(field.value, validation.operand);
			} else {
				fieldIsValid = ruleFunction(field.value);
			}

			if(!fieldIsValid) {
				validator.showMessage(field, validation.message);
				return true;
			} else {
				validator.hideMessage(field);
			}
		});
	},
	showMessage : function(field, message) {
		document.getElementById("validate_"+field.getAttribute("name")).innerHTML = message;
	},
	hideMessage : function(field) {
		document.getElementById("validate_"+field.getAttribute("name")).innerHTML = "";
	}
};

var states = {
	1 : "Punjab",
	2 : "Haryana",
	3 : "Maharashtra"
}

function ajax(state) {
	var cities={
		1:{
			1:"Ferozepur",
			2:"Chandigarh",
			3:"Patiala"
		},
		2:{
			1:"Gurgaon",
			2:"Panchkula"

		},
		3:{
			1:"Pune",
			2:"Mumbai"

		}
	};
	return cities[state];
}

function fetch_cities(field){
	document.getElementById("city").innerHTML="";

	var state = field.value;

	var cities = ajax(state);
	
	for(var city_id in cities ){
		var opt = document.createElement("option");
		var t = document.createTextNode(cities[city_id]);
		opt.appendChild(t);
		document.getElementById("city").appendChild(opt).setAttribute('value',city_id);

	}
}