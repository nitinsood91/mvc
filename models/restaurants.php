<?php



class restaurants {
	public static function getCategories(){
		global $connection;
		$stmt=$connection->prepare("select * from categories");
		$stmt->execute();
		$category=$stmt->get_result();
		$resultData=array();
		while($row=$category->fetch_assoc()){
			$resultData[]=$row;
			
			
		}
		return $resultData;


	}
	public static function getRestaurants($parameters){
		global $connection;
			
			
				$stmt=$connection->prepare("select * from restaurants where category_id=?");
				$stmt->bind_param("s",$parameters["category_id"]);
				$stmt->execute();
				$resultData=array();
				$result=$stmt->get_result();
				while($row=$result->fetch_assoc()){
					$resultData[]=$row;
				}
			
			return $resultData;
	}
	public static function getProducts($parameters){
		global $connection;
			$stmt=$connection->prepare("select * from products where restaurant_id=? and category_id=?");
			$stmt->bind_param("ss",$parameters["restaurant_id"],$parameters["category_id"]);
			$stmt->execute();
			$resultData=array();
			$result=$stmt->get_result();
			while($row=$result->fetch_assoc()){
			$resultData[]=$row;
			}
			return $resultData;
	}
	public static function getSizeVariants($parameters){
		global $connection;
		$stmt=$connection->prepare("select * from size_variants where product_id=? and restaurant_id=? and category_id=? ");
		$stmt->bind_param("sss",$parameters["product_id"],$parameters["restaurant_id"],$parameters["category_id"]);
		$stmt->execute();
		$result=$stmt->get_result();
		$resultData=array();
		while($row=$result->fetch_assoc()){
			$resultData[]=$row;
			
		}
		return $resultData;


	}
	public static function getModifiers($parameters){
		global $connection;
		$stmt=$connection->prepare("select * from modifiers where size_variant_id=? and product_id=? and restaurant_id=? and category_id=? ");
		$stmt->bind_param("ssss",$parameters["size_variant_id"],$parameters["product_id"],$parameters["restaurant_id"],$parameters["category_id"]);
		$stmt->execute();
		$result=$stmt->get_result();
		$resultData=array();
		while($row=$result->fetch_assoc()){
			$resultData[]=$row;
			
		}
		return $resultData;


	}


}
// -----------------------------------------------------------------------------------------------------
// class restaurant {
// 	public static function getCategories(){
// 		global $connection;
// 		$stmt=$connection->prepare("select * from categories");
// 		$stmt->execute();
// 		$category=$stmt->get_result();
// 		$resultData=array();
// 		while($row=$category->fetch_assoc()){
// 			$row["restaurants"]=restaurants::getRestaurants($row);
// 			$resultData[]=$row;
			
			
// 		}
// 		return $resultData;


// 	}
// 	public static function getRestaurants($parameters){
// 		global $connection;
// 			$stmt=$connection->prepare("select * from restaurants where category_id=?");
// 			$stmt->bind_param("s",$parameters["category_id"]);
// 			$stmt->execute();
// 			$resultData=array();
// 			$result=$stmt->get_result();
// 			while($row=$result->fetch_assoc()){
// 				$row["products"]=restaurants::getProducts($row);
// 			$resultData[]=$row;
// 			}
// 			return $resultData;
// 	}
// 	public static function getProducts($parameters){
// 		global $connection;
// 			$stmt=$connection->prepare("select * from products where restaurant_id=? and category_id=?");
// 			$stmt->bind_param("ss",$parameters["restaurant_id"],$parameters["category_id"]);
// 			$stmt->execute();
// 			$resultData=array();
// 			$result=$stmt->get_result();
// 			while($row=$result->fetch_assoc()){
// 				$row["size_variants"]=restaurants::getSizeVariants($row);
// 			$resultData[]=$row;
// 			}
// 			return $resultData;
// 	}
// 	public static function getSizeVariants($parameters){
// 		global $connection;
// 		$stmt=$connection->prepare("select * from size_variants where product_id=? and restaurant_id=? and category_id=? ");
// 		$stmt->bind_param("sss",$parameters["product_id"],$parameters["restaurant_id"],$parameters["category_id"]);
// 		$stmt->execute();
// 		$result=$stmt->get_result();
// 		$resultData=array();
// 		while($row=$result->fetch_assoc()){
// 			$row["modifiers"]=restaurants::getModifiers($row);
// 			$resultData[]=$row;
			
// 		}
// 		return $resultData;


// 	}
// 	public static function getModifiers($parameters){
// 		global $connection;
// 		$stmt=$connection->prepare("select * from modifiers where size_variant_id=? and product_id=? and restaurant_id=? and category_id=? ");
// 		$stmt->bind_param("ssss",$parameters["size_variant_id"],$parameters["product_id"],$parameters["restaurant_id"],$parameters["category_id"]);
// 		$stmt->execute();
// 		$result=$stmt->get_result();
// 		$resultData=array();
// 		while($row=$result->fetch_assoc()){
// 			$resultData[]=$row;
			
// 		}
// 		return $resultData;


// 	}


// }
?>