<?php 
class register{
	public static function add($parameters){
		global $connection;
		$stmt=$connection->prepare("insert into register(name,email,contact_number,password,state_id,city_id) values(?,?,?,?,?,?)");
		
		$stmt->bind_param("ssisii",$parameters["name"],$parameters["email"],$parameters["contact_number"],$parameters["password"],$parameters["state_id"],$parameters["city_id"]);
		//print_R($stmt); die;
		$stmt->execute();
		return "User Registered Successfully";

	}
}
?>