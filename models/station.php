<?php 
class station{

	public static function add($parameters){
		global $connection;

		$conn = $connection->prepare("insert into stations(station_name,city_name) values(?,?) "); 
		$conn-> bind_param("ss",$parameters["station_name"],$parameters["city_name"]);
		$conn->execute();
		
		return "Metro station added successfully";

	}

}

?>