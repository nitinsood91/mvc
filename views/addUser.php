<html>
	<head>
		<link rel="stylesheet" type="text/css" href="register.css">
	</head>
	<body>
	<script src="http://localhost/delhimetro/public/js/register.js"></script>
		<form action="http://localhost/delhimetro/submit-user" onsubmit="return submitValidation()" method="POST">

			<div class="wrapper">
				<div class="row">
					<div class="col5">
						<div class="field_name">
							Name :  
						</div>
						<div class="input_field">
							<input  id="name" type="text" name="name"  onblur="validateName(this)">
							<p id="validate_name"></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col5">
						<div class="field_name">
							Email Id(Login Id):
						</div>
						<div class="input_field">
							<input id="email" type="text"  name="email" onblur="validateEmail(this)" >
							<p id="validate_email"></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col5">
						<div class="field_name">
							Contact Number:
						</div>
						<div class="input_field">
							<input id="contact_number" type="text"  name="contact_number" onblur="validateContact(this)">
							<p id="validate_contact_number"></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col5">
						<div class="field_name">
							Password:
						</div>
						<div class="input_field">
							<input id="password" type="password"  name="password" onblur="validatePassword(this)">
							<p id="validate_password"></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col5">
						<div class="field_name">
							Confirm Password:
						</div>
						<div class="input_field">
							<input id="confirm_password" type="password" name="confirm_password" onblur="validateConfirmPwd(this)">
							<p id="validate_confirm_password"></p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col5">
						<div class="field_name">
							State:
						</div>
						<div class="input_field">
							<select name="state_id" onchange="fetch_cities(this)">
								<?php foreach($states as $key =>$state){ ?>
									<option value=<?php echo $state["state_id"]; ?>><?php echo $state["state_name"]; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col5">
						<div class="field_name">
							City:
						</div>
						<div class="input_field">
							<select id ="city" name="city_id">
								
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col5">
						<div class="field_name">
							Locality:
						</div>
						<div class="input_field">
							<input type="text" id="locality" name="locality" onblur="validateLocality(this)">
							<p id="validate_locality"></p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col10">
						<div class="submit">
							<input id = "submit" type="submit" value="Register Now" >
					</div>
				</div>


			</div>

		</form>
	</body>
</html>