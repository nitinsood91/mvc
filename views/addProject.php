<html>
	<link rel="stylesheet" href="public/css/addProject.css">
	<head>
		<script type="text/javascript" src="http://localhost/delhimetro/public/js/addProject.js" ></script>
	</head>
	<body>
		<div class="featuredProjects">
			<div class="heading">
				<p>Featured Projects</p>
			</div>
			<div class="project" onmouseover="show(this.querySelector('.tooltip'))" onmouseout="hide(this.querySelector('.tooltip'))">
				<p>Project 1</p>
				<span class="tooltip">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</span>
			</div>
			<div class="project" onmouseover="show(this.querySelector('.tooltip'))" onmouseout="hide(this.querySelector('.tooltip'))">
				<p>Project 2</p>
				<span class="tooltip">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</span>
			</div>
			<div class="project" onmouseover="show(this.querySelector('.tooltip'))" onmouseout="hide(this.querySelector('.tooltip'))">
				<p>Project 3</p>
				<span class="tooltip">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</span>
			</div>
			<div class="project" onmouseover="show(this.querySelector('.tooltip'))" onmouseout="hide(this.querySelector('.tooltip'))">
				<p>Project 4</p>
				<span class="tooltip">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</span>
			</div>
		</div>
	</body>
</html>