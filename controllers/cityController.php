<?php
include_once(ROOT."\models\city.php");
include_once(ROOT."\models\state.php");
// include_once(ROOT."\models\country.php");

class cityController{

	function addCity(){
		$parameters = array(
			"states"  => state::getStates()
			
		);
		return view("addCity",$parameters);
	
	}
	

	function submitCity(){

		$cityData= array(
			"city_name"=>$_POST["city_name"],
			"city_code"=>$_POST["city_code"],
			"state_id"=>$_POST["state_id"]
			);

			return city::add($cityData);

	}
	function cityView(){
		$cities=array(
		"cities"=>city::getcities()
		);
		return view("viewCity",$cities);


	}

	function cityEdit(){
		$city=array(
		"city_id"=>$_GET["city_id"]
		);

		$city_data=city::get($city);

	}
}	
?>