<?php
include_once(ROOT."\models\state.php");
include_once(ROOT."\models\country.php");
class stateController{

	function statePage(){
		return view("statePage");

	}

	function stateSubmit(){
		$stateData=array(
			"state_name"=>$_POST["state_name"],
			
			//"std_code"=>$_POST["std_code"],
			
			);
		

			
				return state::add($stateData);
			    

	}

	function stateView(){
		$state=array(
			"states"=>state::getStates()
		);
		return view("viewState",$state);

	}

	function stateEdit(){
		$parameters=array(
			"state_id"=>$_GET["state_id"]	
			);
		$state=state::get($parameters);
		$countries=country::getCountries();
		$update_state=array(
			"state"=>$state,
			"countries"=>$countries
			);
		
		return view("editState",$update_state);
	}

	function stateUpdate(){
		$parameters=array(
			"state_id"=>$_POST["state_id"],
			"state_name"=>$_POST["state_name"],
			"state_url"=>$_POST["state_url"],
			"country_id"=>$_POST["country_id"],
			);
		$state=state::update($parameters);
		return $state;
	}
}

?>