<?php
include_once(ROOT."\models\\restaurants.php"); 
class menuController{
	function menuPage(){
		return view("menuPage");
	}

	function getMenu(){
		$parameters=array(
			// "restaurant_id"=>$_GET["restaurant_id"]
		); 
		// $menu=restaurants::getMenu($parameters);
		//write
		// $menu= array(
		// 	"categories"=>array(restaurants::getCategories(),
		// 		"restaurants"=>restaurants::getRestaurants())
		// 	);
		$menu = array(
			"categories" => restaurants::getCategories()
		);

		foreach($menu["categories"] as $category_key=>$category){
			$restaurants=restaurants::getrestaurants($category);

			foreach($restaurants as $restaurant_key=>$restaurant){
				$menu["categories"][$category_key]["restaurants"][$restaurant_key]=$restaurant;
				$products=restaurants::getProducts($restaurant);

				foreach($products as $product_key=>$product){
					$menu["categories"][$category_key]["restaurants"][$restaurant_key]["products"][$product_key]=$product;
					$size_variants=restaurants::getSizeVariants($product);

					foreach($size_variants as $size_variant_key=>$size_variant){
						$menu["categories"][$category_key]["restaurants"][$restaurant_key]["products"][$product_key]["size_variants"][$size_variant_key]=$size_variant;
						$modifiers=restaurants::getModifiers($size_variant);

						foreach($modifiers as $modifier_key => $modifier){
							$menu["categories"][$category_key]["restaurants"][$restaurant_key]["products"][$product_key]["size_variants"][$size_variant_key]["modifiers"][$modifier_key]=$modifier;

						}
					}

				}

			}
			
		}
		return json_encode($menu);

	}
}

?>