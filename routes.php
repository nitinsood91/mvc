<?php

$routes=array(
	"create-city"=>"cityController@addCity",
	"submit-city"=>"cityController@submitCity",
	"create-station"=>"stationController@stationAdd",
	"submit-station"=>"stationController@stationSubmit",
	"rider-registration-form"=>"registerController@addUser",
	"submit-user"=>"registerController@submitUser",
	"create-state"=>"cityController@statePage",
	"submit-state"=>"cityController@stateSubmit",
	"view-properties"=>"propertyController@viewProperties",
	"get-city-localities" => "propertyController@getCityLocalities",
	"login"=>"loginController@loginPage",
	"tooltip"=>"miscController@newPage",
	"featured-projects"=>"projectController@projectInfo",
	"display-menu"=>"menuController@menuPage",
	"get-menu"=>"menuController@getMenu"
	);
?>